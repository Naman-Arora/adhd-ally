import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select


url = 'https://adhdallyus.site/'

class Test(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=chrome_options)

    def tearDown(self):
        self.driver.quit()

    def test1(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'About')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/about"))
        self.assertEqual(self.driver.current_url, url + "about")

    def test2(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'States')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states"))
        self.assertEqual(self.driver.current_url, url + "states")

    def test3(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'Professionals')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals"))
        self.assertEqual(self.driver.current_url, url + "professionals")

    def test4(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'Support Groups')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/supportgroups"))
        self.assertEqual(self.driver.current_url, url + "supportgroups")

    def test_states_card(self):
        self.driver.get(url + 'states')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states/Alabama"))
        self.assertEqual(self.driver.current_url, url + "states/Alabama")


    def test_professionals_card(self):
        self.driver.get(url + 'professionals')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals/006587a7-ad15-4e6d-8e12-30f4b6e0ab2d"))
        self.assertEqual(self.driver.current_url, url + "professionals/006587a7-ad15-4e6d-8e12-30f4b6e0ab2d")

    def test_supportgroups_card(self):
        self.driver.get(url + 'supportgroups')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/supportgroups/00cdc13b-0171-4bd5-ad7c-54525f3abd0e"))
        self.assertEqual(self.driver.current_url, url + "supportgroups/00cdc13b-0171-4bd5-ad7c-54525f3abd0e")
        
    def test_states_home(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/main/section[3]/div/div/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.execute_script("arguments[0].click();", button)
        # button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states"))
        self.assertEqual(self.driver.current_url, url + "states")
        
    def test_profs_home(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/main/section[4]/div/div/div[2]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals"))
        self.assertEqual(self.driver.current_url, url + "professionals")

    def test_home_navbar(self):
        self.driver.get(url + 'states')
        self.driver.implicitly_wait(20)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.LINK_TEXT, 'ADHDAlly')))
        button = self.driver.find_element(By.LINK_TEXT, 'ADHDAlly')
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.invisibility_of_element_located((By.LINK_TEXT, 'states')))
        self.assertEqual(self.driver.current_url, url)





if __name__ == "__main__":
    unittest.main()
