export const PATHS = [
  {
    name: "States",
    path: "/states",
  },
  {
    name: "Professionals",
    path: "/professionals",
  },
  {
    name: "Support Groups",
    path: "/supportgroups",
  },
  {
    name: "About",
    path: "/about",
  },
  {
    name: "Visualizations",
    path: "/visualizations",
  },
   {
    name: "Developer Visualizations",
    path: "/developervisualizations",
  },
] as const;
