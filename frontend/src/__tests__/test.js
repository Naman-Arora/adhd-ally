import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import About from "../app/about/page.tsx"
import Home from "../app/page.tsx"
import Professionals from "../app/professionals/Professionals.tsx"
import States from "../app/states/page.tsx"
import SupportGroups from "../app/supportgroups/SupportGroups.tsx"

import ProfessionalInstance from "../app/professionals/[id]/page.tsx"
import StateInstance from "../app/states/[id]/page.tsx"
import SupportGroupInstance from "../app/supportgroups/[id]/page.tsx"

import ImageCard from "../components/ImageCard.tsx"
import NavBar from "../components/NavBar.tsx"

describe('Main Pages', () => {
   // Unit Test 1
   test('About Page', () => {
      const tree = <BrowserRouter>renderer.create(<About />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })

   // Unit Test 2
   test('Home Page', () => {
      const tree = <BrowserRouter>renderer.create(<Home />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })

   // Unit Test 3
   test('Professionals Page', () => {
      const tree = <BrowserRouter>renderer.create(<Professionals />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })

   // Unit Test 4
   test('States Page', () => {
      const tree = <BrowserRouter>renderer.create(<States />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })

   // Unit Test 5
   test('Support Groups Page', () => {
      const tree = <BrowserRouter>renderer.create(<SupportGroups />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })
})

describe('Instance Pages', () => {
   // Unit Test 6
   test('Professional Instance', () => {
      const tree = <BrowserRouter>renderer.create(<ProfessionalInstance />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })

   // Unit Test 7
   test('State Instance', () => {
      const tree = <BrowserRouter>renderer.create(<StateInstance />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })

   // Unit Test 8
   test('Support Group Instance', () => {
      const tree = <BrowserRouter>renderer.create(<SupportGroupInstance />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   })
})

describe('Components', () => {
   // Unit Test 9
   test('Image Card', () => {
      const tree = <BrowserRouter>renderer.create(<ImageCard />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   });

   // Unit Test 10
   test('Navigation Bar', () => {
      const tree = <BrowserRouter>renderer.create(<NavBar />).toJSON()</BrowserRouter>
      expect(tree).toMatchSnapshot()
   });
})
