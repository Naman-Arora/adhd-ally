# ADHD Ally US - Group 15

### Team Member Information

| Name               | GitLab ID        | EID     |
|:------------------ | ---------------- | ------- |
| Abdallah Al-Sukhni | @doogls          | aha2242 |
| Naman Arora        | @naman-arora     | na26896 |
| Haoyu Fan          | @haoyujfan       | hf5542  |
| Faith Nguyen       | @fpnguyen        | pnn329  |
| Tejaswi Thapa      | @tejaswithapa    | tt26788 |

### Website

[ADHD Ally](https://adhdallyus.site/)

### Backend API

[ADHD Ally API](https://api.adhdallyus.site/)

### API Documentation

[Postman](https://documenter.getpostman.com/view/32958006/2sA2r53kYu)

### Estimated/Actual Completion Time

| Phase | Member             | Estimated (hrs) | Actual (hrs) |
| ----- | ------------------ | --------------- | ------------ |
| 1     | Abdallah Al-Sukhni | 20              |   25         |
|       | Naman Arora        | 20              |   25         |
|       | Haoyu Fan          | 20              |   25         |
|       | Faith Nguyen       | 21              |   25         |
|       | Tejaswi Thapa      | 20              |   25         |
| 2     | Abdallah Al-Sukhni | 30              |   35         |
|       | Naman Arora        | 30              |   35         |
|       | Haoyu Fan          | 30              |   35         |
|       | Faith Nguyen       | 30              |   35         |
|       | Tejaswi Thapa      | 30              |   35         |

### Project Leader

| Phase |    Phase Leader    |
| ----- | ------------------ |
| 1     |   Faith Nguyen     |
| 2     |   Naman Arora      |
| 3     |                    |
| 4     |                    |

**Phase Leader Responsibilities:** Effectively communciate with team members, set up meeting times, keep track of progress, and distribute work evenly.

### GitLab Pipelines

[Pipelines](https://gitlab.com/tejaswithapa/cs373-group-15/-/pipelines)

### Git SHA

| Phase |                 SHA                       |
| ----- | ----------------------------------------- |
| 1     | eb5faf1fb321d9a24876b185b201e9965b371594  |
| 2     | 93a85199197645fb8b4516e66ab002dd63456778  |
| 3     |                                           |
| 4     |                                           |

### Comments
