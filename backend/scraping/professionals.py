import requests
from bs4 import BeautifulSoup
import json

# Define a set of all 50 US states in lowercase
states_lower = {
    'alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado',
    'connecticut', 'delaware', 'florida', 'georgia', 'hawaii', 'idaho',
    'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana',
    'maine', 'maryland', 'massachusetts', 'michigan', 'minnesota',
    'mississippi', 'missouri', 'montana', 'nebraska', 'nevada',
    'new hampshire', 'new jersey', 'new mexico', 'new york',
    'north carolina', 'north dakota', 'ohio', 'oklahoma', 'oregon',
    'pennsylvania', 'rhode island', 'south carolina', 'south dakota',
    'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington',
    'west virginia', 'wisconsin', 'wyoming',
}
states_dict = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
}

# Use set comprehension to create a new set that includes all states in both lowercase and uppercase
states = {state for state in states_lower} | {state.capitalize() for state in states_lower}

# Function to find a state in an address
def find_state_in_address(address):
    # Split the address into words
    words = address.split()
    for i in range(len(words)):
        words[i] = words[i].strip()
        words[i] = words[i].strip(',')
        words[i] = words[i].strip('.')
    # Try to match each word (and consecutive pairs of words for two-word state names) against the states set
    for i, word in enumerate(words):
        # Check single word state
        if word in states:
            return word.capitalize()
        # Check two-word state
        if i < len(words) - 1:
            two_word_state = f"{word.capitalize()} {words[i+1].capitalize()}"
            if two_word_state in states:
                return two_word_state
        if word.upper() in states_dict:
            return states_dict[word.upper()]
    return None

HEADERS = {'User-Agent': 'Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'}
extracted_listings = []
x = 1

for i in range(1, 40) :
    # get URL and HTML
    url = "https://add.org/professional-directory/page/" + str(i) + "/?order_by=title&order=ASC"
    response = requests.get(url, headers=HEADERS)
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all article tags which are assumed to represent each listing
    # Adjust the selector as needed to match your HTML structure
    listings = soup.find_all('article')

    for listing in listings:
        listing_data = {
            "id_number": str(x),
            "name": listing.find('h2').get_text(strip=True) if listing.find('h2') else "Data not found",
            "data": {
                "title": "Data not found",
                "organization": "Data not found",
                "location": "Data not found",
                "state": "Data not found",
                "phone": "Data not found",
                "remoteWork": "No"
            },
            "flag": "https://st4.depositphotos.com/14953852/22772/v/450/depositphotos_227724992-stock-illustration-image-available-icon-flat-vector.jpg",
            "url": listing.find('a')['href'] if listing.find('a') else "Data not found",
            "categories": [],
            "tags": [],
            "addresses": []
        }

        # Extract title
        if listing.find('div', class_='w2dc-field w2dc-field-output-block w2dc-field-output-block-string w2dc-field-output-block-13'):
            listing_data['data']['title'] = listing.find('div', class_='w2dc-field w2dc-field-output-block w2dc-field-output-block-string w2dc-field-output-block-13').span.text.strip()

        # Extract organization
        if listing.find('div', class_='w2dc-field w2dc-field-output-block w2dc-field-output-block-string w2dc-field-output-block-14'):
            listing_data['data']['organization'] = listing.find('div', class_='w2dc-field w2dc-field-output-block w2dc-field-output-block-string w2dc-field-output-block-14').span.text.strip()
        
        # Assuming categories and tags are under specific elements, adjust as necessary
        categories_and_tags = listing.find_all('a', rel='tag')
        for item in categories_and_tags:
            if 'category' in item['href']:
                listing_data['categories'].append(item.get_text(strip=True))
                if item.get_text(strip=True) == "Works Remotely" :
                    listing_data['data']['remoteWork'] = "Yes"
            else:
                listing_data['tags'].append(item.get_text(strip=True))

        # Assuming phone numbers are in a specific format or contain identifiable markers
        phone = listing.find(text=lambda x: "Phone:" in x)
        if phone:
            listing_data['data']['phone'] = phone.find_next('a').get_text(strip=True) if phone.find_next('a') else None

        # Assuming addresses might be marked by itemprop attribute or similar
        addresses = listing.find_all(itemprop='address')
        for address in addresses:
            listing_data['addresses'].append(address.get_text(strip=True))
            listing_data['data']['location'] = address.get_text(strip=True)
            listing_data['data']['state'] = find_state_in_address(address.get_text(strip=True))

        # Extract image
        if listing.find('div', class_='w2dc-listing-logo-img'):
            img_tag = listing.find('div', class_='w2dc-listing-logo-img').find('img')
            if img_tag and img_tag.has_attr('src'):
                image_url = img_tag['src']
                listing_data['flag'] = image_url

        extracted_listings.append(listing_data)
        x += 1

# Save the extracted data to a JSON file
with open('professionals_data.json', 'w') as json_file:
    json.dump(extracted_listings, json_file, indent=4, ensure_ascii=False)

print("Extraction completed and saved to listings_extracted.json.")