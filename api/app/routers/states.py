from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import APIRouter, HTTPException, Depends
from app.dependencies import get_db, get_params

router = APIRouter(
    prefix="/states",
    tags=["states"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def getAll(
    query: dict[str, int] = Depends(get_params),
    session: AsyncSession = Depends(get_db),
):
    res = await session.execute(
        text(
            "SELECT * from states ORDER BY name LIMIT :limit OFFSET :offset"
        ).bindparams(
            limit=query["limit"], offset=(query["limit"] * (query["page"] - 1))
        )
    )
    return [x._asdict() for x in res.all()]


@router.get("/metadata")
async def metadata(
    query: dict[str, int] = Depends(get_params), session: AsyncSession = Depends(get_db)
):
    res = await session.execute(text("SELECT COUNT(*) from states"))
    return {"count": res.scalar(), "limit": query["limit"]}


@router.get("/{name}")
async def get(name: str, session: AsyncSession = Depends(get_db)):
    res = await session.execute(
        text("SELECT * from states WHERE name=:name  ORDER BY name").bindparams(
            name=name
        )
    )

    data = res.first()

    if not data:
        raise HTTPException(status_code=404, detail="Not found :(")

    return data._asdict()
